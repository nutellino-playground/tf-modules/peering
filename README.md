# Peering module

Original project: https://github.com/cloudposse/terraform-aws-vpc-peering.git

## Usage

```hcl
module "vpc_peering" {
  source           = "git::https://gitlab.com/nutellino-playground/tf-modules/peering.git?ref=0.0.1"
  namespace        = "cp"
  stage            = "dev"
  name             = "cluster"
  requestor_vpc_id = "vpc-XXXXXXXX"
  acceptor_vpc_id  = "vpc-YYYYYYYY"
}
```